---
title: "make_physeq.RMD"
author: "XCM"
date: "4/14/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(dada2)
library(phyloseq)
```

Overview of dada2 workflow with this data: 

Rerun 454 data with dada2
1) Use mothur (mothur v.1.25.0) to make fastqs from original 454 data -> requires local mothur install. Run these commands from primary data files to make fastqs.

make.fastq(fasta=Ksimpson-pr.fasta, qfile=Ksimpson-pr.qual)
make.fastq(fasta=Ksimpson-full.fasta, qfile=Ksimpson-full.qual)

Dada2 requires multiplexing. Use this script 
2) Use perl script (/src/demultiplex-fastq.pl) to demultiplex into individual fastq files, which are parked in "demultiplex".
 
 3) Do dada2 stuff, make physeq
 

```{r, echo=FALSE}
pathF <- "~/Simpson2021/primary_data/demultiplex" # CHANGE ME to the directory containing your demultiplexed forward-read fastqs
filtpathF <- file.path(pathF, "filtered") # Filtered forward files go into the pathF/filtered/ subdirectory
fastqFs <- sort(list.files(pathF, pattern="fastq"))

sample.names <- sapply(strsplit(fastqFs, ".fastq"), `[`, 1)


out<-filterAndTrim(fwd=file.path(pathF, fastqFs), filt=file.path(filtpathF, fastqFs),
              truncLen=300, maxEE=2, truncQ=11, rm.phix=TRUE,
              compress=TRUE, verbose=TRUE, multithread=TRUE)


# File parsing
filtpathF <- "~/Simpson2021/primary_data/demultiplex/filtered" # CHANGE ME to the directory containing your filtered forward fastqs

filtFs <- list.files(filtpathF, pattern="fastq", full.names = TRUE)
names(filtFs) <- sample.names
set.seed(100)
# Learn forward error rates
errF <- learnErrors(filtFs, nbases=1e8, multithread=TRUE)

dds <- vector("list", length(sample.names))
names(dds) <- sample.names
for(sam in sample.names) {
  cat("Processing:", sam, "\n")
  derepF <- derepFastq(filtFs[[sam]])
  dds[[sam]] <- dada(derepF, err=errF, HOMOPOLYMER_GAP_PENALTY=-1, BAND_SIZE=32, multithread=TRUE)
}
# Construct sequence table and write to disk
seqtab <- makeSequenceTable(dds)
# Remove chimeras
seqtab <- removeBimeraDenovo(seqtab, method="consensus", multithread=TRUE)


# Assign taxonomy
tax <- assignTaxonomy(seqtab, "~/Simpson2021/src/silva_v132/silva_nr_v132_train_set.fa.gz", multithread=TRUE)

meta<-read.table("~/Simpson2021/primary_data/Fasting_Map2.txt.qiime", header=TRUE, sep="\t")
rownames(meta) = meta$SampleID
meta$Disease = as.factor(meta$Disease)
meta$Composite = meta$Disease
meta$Composite = gsub("Healthy", "non-IBD", meta$Composite)




physeq<-phyloseq(otu_table(seqtab, taxa_are_rows = FALSE), tax_table(tax), sample_data(meta))

# Write to disk
saveRDS(physeq, "~/Simpson2021/primary_data/kenny-dada2.rds") # CHANGE ME to where you want sequence table 

#ps is the phyloseq object without a phylogenetic tree
#Phylogenetic tree using Decipher and phangorn
library(DECIPHER)
library(phangorn)
library(Biostrings)
seqs<-getSequences(colnames(otu_table(ks_dada)))
names(seqs)<-seqs
alignment <- DECHIPER::AlignSeqs(DNAStringSet(seqs), anchor=NA)
phang.align <- phyDat (as (alignment, "matrix"), type = "DNA")
dm <- dist.ml(phang.align)
treeNJ <- NJ(dm)
fit = pml(treeNJ, data = phang.align)
fitGTR <- update(fit, k=4, inv=0.2)
fitGTR <- optim.pml(fitGTR, model="GTR", optInv=TRUE, optGamma = TRUE, rearrangement = "stochastic", control = pml.control(trace = 0))
bs = bootstrap.pml(fitGTR, bs=100, optNni=TRUE, multicore=TRUE)
ps <- phyloseq(otu_table(ks_dada), tax_table(ks_dada), sample_data(ks_dada), phy_tree(fitGTR$tree))
saveRDS(ps, "~/Simpson2021/primary_data/kenny-dada2.rds") # CHANGE ME to where you want sequence table
```
